import { Request, Response, Router } from 'express';
import { cfg } from '../cfg';
import { layoutVariables } from '../layout';

export const router_landing = Router();

/* GET home page. */
router_landing.get('/', function(req: Request, res: Response) {
  res.render('landing', {
    ...layoutVariables,
  });
});
